import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Fryzjer extends Thread {
    private Semaphore fryzjerSemaphore;
    private Semaphore poczekalniaZmianySemaphore;
    private Semaphore klientSemaphore;
    private int numerKlienta = 0;
    private Klient strzyzonyKlient;

    private List<Klient> listaKlientowWPocz = new LinkedList<>();

    public Fryzjer(Semaphore fryzjerSemaphore, Semaphore poczekalniaZmianySemaphore, Semaphore klientSemaphore,
                   List<Klient> listaKlientowWPocz) {
        this.fryzjerSemaphore = fryzjerSemaphore;
        this.poczekalniaZmianySemaphore = poczekalniaZmianySemaphore;
        this.klientSemaphore = klientSemaphore;
        this.listaKlientowWPocz = listaKlientowWPocz;
    }

    @Override
    public void run() {
        while (true){
            try {
                //fryzjer zaczyna od sprawdzenia czy sa dostepni klienci; jesli nie ma to spi
                klientSemaphore.acquire();

                //mam dostep do klienta, wiec zabieram go z poczekalni
                poczekalniaZmianySemaphore.acquire();   //zablokowanie mozliwosci dodania klienta do poczekalni
                numerKlienta = listaKlientowWPocz.get(0).getMyId();  //pobierz id klienta przed usunieciem z poczekalni
                strzyzonyKlient = listaKlientowWPocz.get(0);
                listaKlientowWPocz.remove(0);      //klient ktory przyszedl pierwszy - jest na 0 pozycji na liscie
                System.out.println("Klient o nr id" + numerKlienta + " wychodzi z poczekalni");
                System.out.println("W poczekalni jest teraz: " + listaKlientowWPocz.size() + " klientow");
                poczekalniaZmianySemaphore.release();

                fryzjerSemaphore.release();             //fryzjer jest dostepny
                strzyzenie();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private int czasStrzyzenia(){
        Random random = new Random();
        return random.nextInt(2000) + 3000;    //wylosowanie czasu strzyzenia od 3 do 5 sekund
    }

    private void strzyzenie (){
        try {
            System.out.println("Zaczynam strzyc klienta o nr id: " + numerKlienta);
            sleep(czasStrzyzenia());
            System.out.println("Skonczylem strzyc klienta o nr id" + numerKlienta);
            strzyzonyKlient.setObciety(true);
        } catch (InterruptedException ex) {
        }
    }

}
