import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Main {
    public static void main (String args []){
        final int pojPocz = 5; //pojemnosc poczekalni
        final int liczbaKlientow = 20;  //liczba wszystkich klientow obsluzonych w zakladzie

        List<Klient> listaKlientowWPocz = new LinkedList<>();       //lista klientow w poczekalni

        Random random = new Random();

        Semaphore poczekalniaZmianySemaphore = new Semaphore(1);
        Semaphore klientSemaphore = new Semaphore(0);
        Semaphore fryzjerSemaphore = new Semaphore(0);

        //tworze obiekty
        //fryzjer
        Fryzjer fryzjer = new Fryzjer(fryzjerSemaphore, poczekalniaZmianySemaphore, klientSemaphore,
                listaKlientowWPocz);
        //klient
        Klient klientTab [] = new Klient[liczbaKlientow];   //tablica wszystkich klientow
        for (int i = 0; i < liczbaKlientow; i++) {
            klientTab[i] = new Klient(fryzjerSemaphore, poczekalniaZmianySemaphore, klientSemaphore,
                    pojPocz, listaKlientowWPocz);           //stworzenie obiektu
        }

        //tworze watki
        //fryzjer
        fryzjer.start();
        //klienci
        for (int i = 0; i < liczbaKlientow; i++) {
            klientTab[i].start();                             //uruchomienie watku
            int czasSpania = random.nextInt(3000) + 2000;     //czas oczekiwania na nowy watek
                                                                    // od 2000 do 5000 milisekund
            try {
                sleep(czasSpania);
            } catch (InterruptedException ex) {
            }
        }

    }
}
