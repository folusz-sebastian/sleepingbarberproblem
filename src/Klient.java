import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Klient extends Thread {
    private Semaphore fryzjerSemaphore;
    private Semaphore poczekalniaZmianySemaphore;
    private Semaphore klientSemaphore;
    private int pojPocz;
    public static int liczbaKlientow = 0;
    private int myId;
    private boolean obciety;

    private List<Klient> listaKlientowWPocz = new LinkedList<>();

    public Klient(Semaphore fryzjerSemaphore, Semaphore poczekalniaZmianySemaphore, Semaphore klientSemaphore,
                  int pojPocz, List<Klient> listaKlientowWPocz) {
        this.fryzjerSemaphore = fryzjerSemaphore;
        this.poczekalniaZmianySemaphore = poczekalniaZmianySemaphore;
        this.klientSemaphore = klientSemaphore;
        this.pojPocz = pojPocz;
        this.listaKlientowWPocz = listaKlientowWPocz;
        liczbaKlientow++;
        this.myId = liczbaKlientow;
        this.obciety = false;
    }

    @Override
    public void run() {
            try {
                //klient zaczyna od sprawdzenia wolnych miejsc w poczekalni
                poczekalniaZmianySemaphore.acquire();   //zablokowanie mozliwosci usuniecia klienta z
                                                        // poczekalni przez fryzjera
                if (listaKlientowWPocz.size() < pojPocz){

                    listaKlientowWPocz.add(this);   //dodanie klienta do listy klientow w poczekalni
                    System.out.println("Wchodze do poczekalni. " +
                            "Jestem " + (listaKlientowWPocz.lastIndexOf(this)+1) + " osobą");
                    poczekalniaZmianySemaphore.release();

                    klientSemaphore.release();      //powiadomienie fryzjera ze klient jest w poczekalni
                    fryzjerSemaphore.acquire();     //czekanie na dostepnosc fryzjera
                    while (!this.obciety){       //klient czeka na obciecie
                    }
                } else {
                    System.out.println("Nie ma wolnego miejsca w poczekalni");
                    poczekalniaZmianySemaphore.release();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        System.out.println("Klient " + liczbaKlientow + "zostal obciety");
    }

    public int getMyId() {
        return myId;
    }

    public boolean isObciety() {
        return obciety;
    }

    public void setObciety(boolean obciety) {
        this.obciety = obciety;
    }
}
